# The Complete Nodejs Developer Course
## 1. Fundamentals
### Use require to load third-party modules and our own files
    const fs = require(‘fs’);
    const geocode = require(‘./geocode/geocode’);

### Restarting App with Nodemon
instead of

    node app.js

we should use

    nodemon app.js

it will restart the server each time we save the file app.js
#### We can get input from Users with `process.argv`
#### Or we can use Yargs to simplified input
    const yargs = require(‘yargs’);
#### Don’t repeat myself – Remember to refactor my code
#### Always try to reuse my code
### Debugging
with Terminal:

    node inspector app.js
    nodemon inspector app.js

I can set break point by add `debugger` to the place I want
type `c` to continue execute the app and stop at debugger
type `n` to next
Use `repl` when debugging to access variables, instances,…
with Chrome:

    node –inspect-brk app.js

or  nodemon –inspect-brk app.js
then I have to check on Chrome browser

    chrome://inspect

→ choose Node.js inspector

### Arrow function
- with object:
  e.g:

      var user = {
        name: ‘Hai’,
        sayHi: () => {
          console.log(arguments);
          console.log(`Hi. I'm ${this.name}`);
        },
        sayHiAlt () {
          console.log(arguments);
          console.log(`Hi. I'm ${this.name}`);
        }
      }
in this example, `user.sayHi()` won't work as expected because `this` keywords
does not get bounced. Additionally, **regular function** can read *arguments*
-> in this situation we should use **regular function**

Otherwise, we can use **arrow functions** anywhere we can

## 2. Async

Nodejs is a non-blocking IO model, single thread, even-driven. It's built on *Chrome's V8 JavaScript engine*

### Call Stack & Event Loop

Watch again this section sometimes

### Pretty Printing Objects
    console.log(JSON.stringify(object, undefined, 2));
